﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace WebApplication1
{
    /// <summary>
    /// Summary description for WebService1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WebService1 : System.Web.Services.WebService
    {
        MacierzDataContext db = new MacierzDataContext(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\kasia\Desktop\studia\dotNet\MacierzWEB\WebApplication1\App_Data\MacierzDB.mdf;Integrated Security=True");

        [WebMethod]
        public string Historia()
        {
            var selectQuery = from s in db.operacjes select s;
            string x = "";
            foreach (operacje s in selectQuery)
            {
                x += "Macierz A:\n" + s.macierzA + "\nMacierz B:\n" + s.macierzB + "\nWykonane działanie: " + s.operacja + "\nMacierz wynikowa:\n" + s.macierzC + "\n";
            }
            return x;
        }

        [WebMethod]
        public string DodajDoHistorii(string macierz1, string macierz2, string operacja, string macierz3)
        {
            operacje operation = new operacje();
            operation.macierzA = macierz1;
            operation.macierzB = macierz2;
            operation.operacja = operacja;
            operation.macierzC = macierz3;
            db.operacjes.InsertOnSubmit(operation);
            db.SubmitChanges();
            return "Dodano do historii.";
        }

        [WebMethod]
        public string ZapiszMacierz(int kolumny, int wiersze, string macierz)
        {
            macierz matrix = new macierz();
            matrix.columns = kolumny;
            matrix.rows = wiersze;
            matrix.macierz1 = macierz;
            db.macierzs.InsertOnSubmit(matrix);
            db.SubmitChanges();
            return "Zapisano macierz.";
        }

        [WebMethod]
        public string OdzytMacierzy()
        {
            var selectQuery = from s in db.macierzs select s;
            string matrices = "";
            foreach (macierz s in selectQuery)
            {
                matrices += "ID: " + s.Id + "\n" + s.rows + " " + s.columns + "\n" + s.macierz1 + "\n";
            }
            return matrices;
        }

        [WebMethod]
        public string OdczytPoId(int id)
        {
            var selectQuery = from s in db.macierzs where s.Id == id select s;
            string matrices = "";
            foreach (macierz s in selectQuery)
            {
                matrices += "ID: " + s.Id + "\n" + s.rows + " " + s.columns + "\n" + s.macierz1 + "\n";
            }
            return matrices;
        }
    }
}

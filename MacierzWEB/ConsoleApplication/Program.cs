﻿using System;
using System.IO;
using System.Diagnostics;
using System.Reflection;

namespace ConsoleApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            Macierz.Macierz B= new Macierz.Macierz(4,2,new double[,] { { 1.0, 2.0 }, { 3.0, 4.0 }, { 5.0, 6.0 }, { 7.0, 8.0 } });
            Macierz.Macierz A= new Macierz.Macierz(2,4,new double[,] { { 1.0, 2.0 , 3.0, 4.0 }, { 5.0, 6.0 , 7.0, 8.0 } });
            
            Console.WriteLine("Witam w aplikacji konsolowej Macierze! :)");
            // generowanie menu
            menu(A,B);
            
        }

        static void menu(Macierz.Macierz A, Macierz.Macierz B)
        {           
            Console.WriteLine("Menu:");
            Console.WriteLine("1. Wywołanie funkcji bibliotecznych do operowania na macierzach. ");
            Console.WriteLine("2. Operacje odczytu/zapisu. ");
            Console.WriteLine("3. Wyświetlenie składowych podzespołu .NET.");
            Console.WriteLine("4. Wyświetlenie informacji o autorze/-ach rozwiązania. ");
            Console.WriteLine("5. Wyjście z aplikacji. ");

            string Menu = Console.ReadLine();
            int key;
            if (int.TryParse(Menu, out key))
            {
                switch (key)
                {
                    case 0:
                        menu(A,B);
                        break;
                    case 1:
                        Console.Clear();
                        int r=operacjeNaMacierzach(A, B);
                        Console.WriteLine();
                        if (r == 0)
                            goto case 0;
                        else
                            goto case 1;
                    case 2:
                        Console.Clear();
                        odczytZapis(A,B);
                        goto case 0;
                    case 3:
                        Console.Clear();
                        wyswietlenieSkladowych();
                        goto case 0;
                    case 4:
                        Console.Clear();
                        autorzyRozwiazania();
                        goto case 0;
                    case 5:
                        break;
                    default:
                        Console.WriteLine("Tej opcji nie ma w menu.");
                        goto case 0;
                }
            }
            else
            {
                Console.WriteLine("Niestety ta wartość nic nie robi..");
            }
        }
        

        static void odczytZapis(Macierz.Macierz A, Macierz.Macierz B)
        {
            //Operacje odczytu/zapisu
            Console.WriteLine("Plik znajduje się w katalogu głównym aplikacji.");
            Console.WriteLine("Jaka operacja ma zostać wykonana?");
            Console.WriteLine("O - Odczyt z pliku");
            Console.WriteLine("Z - Zapis do pliku");
            string directory = Directory.GetCurrentDirectory();
            string[] dir = directory.Split('\\');
            directory = "";
            int i = 0;
            while (dir[i] != "ConsoleApplication")
                i++;
            for (int j = 0; j < i; j++)
            {
                directory += dir[j] + '\\';
            }
            directory += "Macierz.txt";
            string key = Console.ReadLine();
            switch (key)
            {
                case "O":
                    //odczyt z pliku;
                    
                    try
                    {
                        using (StreamReader sr = new StreamReader(@directory))
                        {
                            double el;
                            int Acolumn=0, Awiersze=0;
                            string[] lines = File.ReadAllLines(@directory);
                            for(int k = 0; k < lines.Length; k++)
                            {
                                if (lines[k][0].ToString() == "M" && k>0)
                                    Awiersze = k-2;     
                                if (k == 2)
                                    Acolumn = lines[k].Length/2;
                            }
                            A = new Macierz.Macierz(Awiersze, Acolumn, new double[Awiersze,Acolumn]);
                            B = new Macierz.Macierz(lines.Length-4-Awiersze, lines[lines.Length-2].Length/2, new double[lines.Length - 4 - Awiersze,lines[lines.Length - 2].Length/2]);
                            for (i = 0; i < lines.Length-1; i++)
                            {
                                if (double.TryParse(lines[i][1].ToString(), out el))
                                {
                                    string[] elementy = lines[i].Split(new char[] {' '});
                                    for (int j = 1; j < elementy.Length; j++)
                                    {
                                        if (i <= Awiersze + 1)
                                        {
                                            if (!double.TryParse(elementy[j], out A.macierz[i-2, j - 1]))
                                            {
                                                Console.WriteLine("Błąd w pliku!");
                                                return;
                                            }
                                            Console.Write(A.macierz[i-2, j - 1].ToString() + " ");
                                        }else
                                        {
                                            if (!double.TryParse(elementy[j], out B.macierz[i-4-Awiersze, j - 1]))
                                            {
                                                Console.WriteLine("Błąd w pliku!");
                                                return;
                                            }
                                            Console.Write(B.macierz[i -4-Awiersze, j - 1].ToString() + " ");
                                        }
                                    }
                                     
                                    Console.WriteLine();
                                }
                                else
                                {
                                    Console.WriteLine(lines[i]);
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Nie mogę odczytać pliku:");
                        Console.WriteLine(e.Message);
                    }


                    return;
                case "Z":
                    //wyświetlenie zapisywanej macierzy
                    Console.WriteLine("Do pliku zapisano:");
                    Console.WriteLine("Macierz A:");
                    Console.WriteLine("liczba wierszy: " + A.rows + ", liczba kolumn: " + A.columns);
                    for (i = 0; i < A.rows; i++)
                    {
                        for (int j = 0; j < A.columns; j++)
                        {
                            Console.Write(" "+ A[i, j]);
                        }
                        Console.WriteLine();
                    }
                    Console.WriteLine("Macierz B:");
                    Console.WriteLine("liczba wierszy: " + B.rows + ", liczba kolumn: " + B.columns);
                    for (i = 0; i < B.rows; i++)
                    {
                        for (int j = 0; j < B.columns; j++)
                        {
                            Console.Write(" " + B[i, j]);
                        }
                        Console.WriteLine();
                    }

                    // zapis do pliku
                    try
                    {
                        using (StreamWriter writer = new StreamWriter(@directory))
                        {
                            writer.WriteLine("Macierz A");
                            writer.WriteLine("liczba wierszy: "+A.rows + ", liczba kolumn: " + A.columns);
                            string output = "";
                            for (i = 0; i < A.rows; i++)
                            {
                                for (int j = 0; j < A.columns; j++)
                                {
                                    output += String.Format(" "+A[i, j]);
                                }
                                writer.WriteLine(output);
                                output = "";
                            } 
                            writer.WriteLine("Macierz B");
                            writer.WriteLine("liczba wierszy: " + B.rows + ", liczba kolumn: " + B.columns);
                            output = "";
                            for (i = 0; i < B.rows; i++)
                            {
                                for (int j = 0; j < B.columns; j++)
                                {
                                    output += String.Format(" "+B[i, j]);
                                }
                                writer.WriteLine(output);
                                output = "";
                            }
                            writer.WriteLine("UWAGA ! Zmieniając zawartość pliku należy pamiętać, aby cała struktura pliku i wszystkie odstępy były zachowane ! :)");
                            writer.Flush();
                            writer.Close();
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Nie mogę zapisać do pliku:");
                        Console.WriteLine(e.Message);
                    }

                    return;
                default:
                    Console.WriteLine("Nie ma takiej operacji w tym programie :(");
                    return;
            }
        }

        static int operacjeNaMacierzach(Macierz.Macierz A, Macierz.Macierz B)
        {
            
            //Operacje na macierzach
            Console.WriteLine("Dostępne operacje na macierzach:");
            Console.WriteLine("1 - Dodawanie macierzy");
            Console.WriteLine("2 - Odejmnowanie macierzy");
            Console.WriteLine("3 - Mnożenie macierzy");
            Console.WriteLine("4 - Sumowanie wartości wszystkich elementów macierzy A lub B");
            string dzialanie = Console.ReadLine();
            int key;
            Console.WriteLine("Macierz wejściowa A\n");
            for (int i = 0; i < A.rows; i++)
            {
                for (int j = 0; j < A.columns; j++)
                {
                    Console.Write(" "+A[i, j]);
                }
                Console.WriteLine();
            }
            Console.WriteLine("Macierz wejściowa B\n");
            for (int i = 0; i < B.rows; i++)
            {
                for (int j = 0; j < B.columns; j++)
                {
                    Console.Write(" "+B[i, j]);
                }
                Console.WriteLine();
            }
            if (int.TryParse(dzialanie, out key))
            {
                switch (key)
                {
                    case 1:
                        //dodawanie macierzy
                        Macierz.Macierz C;
                        Console.WriteLine("Suma Macierzy:\n");
                        if (A.rows == B.rows && A.columns == B.columns)
                        {
                             C = B + A;
                            for (int i = 0; i < C.rows; i++)
                            {
                                for (int j = 0; j < C.columns; j++)
                                {
                                    Console.Write(" " + C[i, j]);
                                }
                                Console.WriteLine();
                            }
                        }
                        else
                            Console.WriteLine("Niestety nie da się wykonać wybranego działa na tych macierzach.");
                        return 0;
                    case 2:
                        //odejmowanie macierzy

                        Console.WriteLine("Różnica Macierzy A-B \n");
                        if (A.rows == B.rows && A.columns == B.columns)
                        {
                            C = A - B;
                            for (int i = 0; i < C.rows; i++)
                            {
                                for (int j = 0; j < C.columns; j++)
                                {
                                    Console.Write(" " + C[i, j]);
                                }
                                Console.WriteLine();
                            }
                            Console.WriteLine("Różnica Macierzy B-A \n");
                            C = B - A;
                            for (int i = 0; i < C.rows; i++)
                            {
                                for (int j = 0; j < C.columns; j++)
                                {
                                    Console.Write(" " + C[i, j]);
                                }
                                Console.WriteLine();
                            }
                        }
                        else
                            Console.WriteLine("Niestety nie da się wykonać wybranego działa na tych macierzach.");
                        return 0;
                    case 3:
                        //mnożenie macierzy
                        Console.WriteLine("mnożenie macierzy A * B\n");
                        if (A.rows == B.columns && A.columns == B.rows)
                        {
                            C = A * B;
                            for (int i = 0; i < C.rows; i++)
                            {
                                for (int j = 0; j < C.columns; j++)
                                {
                                    Console.Write(" " + C[i, j]);
                                }
                                Console.WriteLine();
                            }
                        }
                        else
                            Console.WriteLine("Niestety nie da się wykonać wybranego działa na tych macierzach.");
                        return 0;
                    case 4:
                        Console.WriteLine("Dla jakiej macierzy chcesz zsumować elementy?");
                        Console.WriteLine("A - macierz A");
                        Console.WriteLine("B - macierz B");
                        string macierz = Console.ReadLine();
                        if (macierz == "A")
                        {
                            Console.Write("Suma wartości elementów macierzy A : "+ A.SumaElementow(A));
                            return 0;
                        }
                        else if (macierz == "B")
                        {
                            Console.Write("Suma wartości elementów macierzy B : "+B.SumaElementow(B));
                            return 0;
                        }
                        else
                            Console.WriteLine("Nie ma takiej macierzy w bazie... :(");
                        return -1;
                    default:
                        Console.WriteLine("Niestety nie wiem o co chodzi... Spróbuj ponownie! ");
                        return -1;
                }
            }
            else
            {
                Console.WriteLine("Aby wybrać pozycję z menu należy wpisać cyfrę, a nie literę.");
                return -1;
            }
        }

        static void wyswietlenieSkladowych()
        {
            //Wyświetlenie składowych podzespołu .NET
            Console.WriteLine("Składowe podzespołu .Net");    
            string version = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            Console.WriteLine("Version: "+version);
            var versionInfo = FileVersionInfo.GetVersionInfo(Assembly.GetEntryAssembly().Location);
            var companyName = versionInfo.CompanyName;
            Console.WriteLine(versionInfo);
            Console.WriteLine(companyName);
            Console.WriteLine();
        }

        static void autorzyRozwiazania()
        {
            // wyświetlenie autora aplikacji
            Console.WriteLine("Autor:");
            Console.WriteLine("Katarzyna Toporek");
            Console.WriteLine("Informatyka 2, semestr II, sekcja 4");
            Console.WriteLine();
        }
    }
}
